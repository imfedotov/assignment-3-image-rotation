#ifndef FORMAT_BMP_H_
#define FORMAT_BMP_H_

#include <stdint.h>
#include <stdio.h>

#include "image.h"

typedef uint16_t u16;
typedef uint32_t u32;
typedef int32_t i32;

typedef struct __attribute__((packed)) {
  u16 Type;
  u32 Size;
  u16 Reserved1;
  u16 Reserved2;
  u32 OffBits;
} BitmapFileHeader;

typedef struct __attribute__((packed)) {
  u32 Size;
  i32 Width;
  i32 Height;
  u16 Planes;
  u16 BitCount;
  u32 Compression;
  u32 SizeImage;
  i32 XPelsPerMeter;
  i32 YPelsPerMeter;
  u32 ClrUsed;
  u32 ClrImportant;
} BitmapInfoHeader;

typedef struct __attribute__((packed)) {
  BitmapFileHeader FileHeader;
  BitmapInfoHeader InfoHeader;
} BitmapHeader;


int ParseBitmapImage(const char *data, const size_t data_len, Image *image);

int WriteImageBitmap(const Image *image, char **out, size_t *size);

#endif // FORMAT_BMP_H_
