#ifndef TRANSFORM_BASIC_H_
#define TRANSFORM_BASIC_H_

#include <stdint.h>

#include "image.h"

void FlipVertical(Image *image);

void FlipHorizontal(Image *image);

void Transpose(Image *image);

#endif // TRANSFORM_BASIC_H_
