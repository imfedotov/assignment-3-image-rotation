#ifndef TRANSFORM_ROTATE_H_
#define TRANSFORM_ROTATE_H_

#include "image.h"

#include "transform/basic.h"

static void RotateBy(Image *image, int degrees) {
  switch (degrees) {
    case 90:
    case -270:
      Transpose(image);
      FlipHorizontal(image);
      break;
    case 180:
    case -180:
      FlipHorizontal(image);
      FlipVertical(image);
      break;
    case 270:
    case -90:
      Transpose(image);
      FlipVertical(image);
      break;
  }
}

#endif // TRANSFORM_ROTATE_H_
