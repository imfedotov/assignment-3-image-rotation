#ifndef IMAGE_H_
#define IMAGE_H_

#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef union {
  struct {
    uint8_t B;
    uint8_t G;
    uint8_t R;
    uint8_t A;  // kek
  };
  uint32_t LongValue;
} Pixel;

typedef struct {
  size_t Width;
  size_t Height;
  Pixel *Data;
} Image;

// Allocate space for image data. 
// Does not allocate if `width` or `height` is zero.
static inline int InitImage(Image *image, size_t width, size_t height) {
  image->Width = width;
  image->Height = height;
  if (width && height) { 
    image->Data = calloc(width * height, sizeof(image->Data[0]));
    if (image->Data == NULL) {
      fputs("InitImage(): calloc() failed", stderr);
      return 1;
    }
  } else {
    // do not attempt malloc(0) (UB)
    // if some faulty logic ignores zero dimension(s)
    // a segfault will occur in testing
    image->Data = NULL;
  }
  return 0;
}

// Use this to free any Image's data
static inline void FreeImageData(Image *image) {
  // avoid double free
  if (image->Data != NULL) {
    free(image->Data);
    image->Data = NULL;
  }
}

#endif // IMAGE_H_
