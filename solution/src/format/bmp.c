#include <arpa/inet.h>
/* #include <stddef.h> */

#include "format/bmp.h"

#include "image.h"

int ParseBitmapImage(const char *data, const size_t data_len, Image *image) {
  BitmapHeader header = *(BitmapHeader *)data;

  if (htons(header.FileHeader.Type) != 0x424d) {
    fputs("ParseBitmapImage(): not a 'BM' file", stderr);
    return 1;
  }

  if (header.FileHeader.OffBits >= data_len) {
    fputs("ParseBitmapImage(): index passed end of buffer\n", stderr);
    return 1;
  }
  // skip to data bytes
  data += header.FileHeader.OffBits;

  if (InitImage(image, header.InfoHeader.Width, header.InfoHeader.Height)) {
    return 1;
  }

  size_t index = 0;

  for (size_t y = 0; y < image->Height; y++) {
    for (size_t x = 0; x < image->Width; x++) {
      if (index + 2 >= data_len) {
        fputs("ParseBitmapImage(): index passed end of buffer\n", stderr);
        return 1;
      }

      Pixel p = {
          .R = data[index + 2],
          .G = data[index + 1],
          .B = data[index],
          .A = 0,
      };
      image->Data[y * image->Width + x] = p;
      index += 3;
    }

    index = 
      index % 4 == 0 
      ? index 
      : index + 4 - index % 4;
  }

  return 0;
}

int WriteImageBitmap(const Image *image, char **out, size_t *size) {
  size_t to_pad = (image->Width * 3) % 4;
  to_pad = to_pad == 0 ? 4 : to_pad;
  size_t lines_size = (image->Width * 3 + 4 - to_pad) * image->Height;
  size_t file_size = sizeof(BitmapHeader) + lines_size;

  // Over-allocate here to type-pun Pixel.LongValue
  char *data = calloc(file_size + 3, 1);
  if (data == NULL) {
    fputs("WriteImageBitmap(): calloc() failed\n", stderr);
    return 1;
  }

  BitmapHeader header = {
    .FileHeader =
    {
      .Type = __bswap_constant_16(0x424d),
      .Size = file_size,
      .Reserved1 = 0,
      .Reserved2 = 0,
      .OffBits = sizeof(BitmapHeader),
    },
    .InfoHeader =
    {
      .Size = sizeof(BitmapInfoHeader),
      .Width = (ssize_t) image->Width,  // NOLINT 
      .Height = (ssize_t) image->Height,  // NOLINT
      .Planes = 1,
      .BitCount = 24,
      .Compression = 0,
      .SizeImage = lines_size,
      .XPelsPerMeter = 11811,
      .YPelsPerMeter = 11811,
      .ClrUsed = 0,
      .ClrImportant = 0,
    },
  };

  *(BitmapHeader *)data = header;
  char *lines = data + sizeof(header);
  size_t index = 0;

  for (size_t y = 0; y < image->Height; y++) {
    for (size_t x = 0; x < image->Width; x++) {
      *(Pixel *)&lines[index] = image->Data[y * image->Width + x];
      index += 3;
    }

    index = 
      index % 4 == 0 
      ? index 
      : index + 4 - index % 4;
  }

  *out = data;
  *size = file_size;

  return 0;
}
