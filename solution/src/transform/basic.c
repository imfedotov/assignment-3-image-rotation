#include "image.h"
#include "transform/basic.h"

void FlipVertical(Image *image) {
  Pixel *data = (Pixel *)image->Data;
  Pixel tmp;

  for (size_t y = 0; y < image->Height; y++) {
    for (size_t x1 = 0, x2 = image->Width - 1; x1 < image->Width / 2; x1++, x2--) {
      tmp = data[y * image->Width + x1];
      data[y * image->Width + x1] = data[y * image->Width + x2];
      data[y * image->Width + x2] = tmp;
    }
  }
}

void FlipHorizontal(Image *image) {
  Pixel *data = image->Data;
  Pixel tmp;

  for (size_t y1 = 0, y2 = image->Height - 1; y1 < image->Height / 2; y1++, y2--) {
    for (size_t x = 0; x < image->Width; x++) {
      tmp = data[y1 * image->Width + x];
      data[y1 * image->Width + x] = data[y2 * image->Width + x];
      data[y2 * image->Width + x] = tmp;
    }
  }
}

void Transpose(Image *image) {
  size_t new_width = image->Height;
  size_t new_height = image->Width;

  Pixel *data = image->Data;
  
  Image new_image;
  InitImage(&new_image, new_width, new_height);

  for (size_t y = 0; y < image->Height; y++) {
    for (size_t x = 0; x < image->Width; x++) {
      new_image.Data[x * image->Height + y] = data[y * image->Width + x];
    }
  }

  FreeImageData(image);
  *image = new_image;
}

