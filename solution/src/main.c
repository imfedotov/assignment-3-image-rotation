#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "format/bmp.h"
#include "image.h"
#include "transform/rotate.h"

static char *ReadFile(const char *filename, size_t *fsize) {
  errno = 0;
  FILE *fp = fopen(filename, "rb");
  if (fp == NULL) {
    fprintf(stderr, "Can't open %s for reading: %s\n", filename, strerror(errno));
    exit(1);
  }

  // SEEK_END might not be supported for given file (e. g. /dev/stdin)
  if (fseek(fp, 0, SEEK_END)) {
    fprintf(stderr, "Can't determine file %s size: %s\n", filename, strerror(errno));
  }
  size_t size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  char *buf = malloc(size);
  if (buf == NULL) {
    fputs("malloc() failed\n", stderr);
    fclose(fp);
    exit(42);
  }

  fread(buf, 1, size, fp);
  fclose(fp);

  *fsize = size;
  return buf;
}

static void WriteFile(const char *filename, const char *buf, size_t size) {
  errno = 0;
  FILE *fp = fopen(filename, "wb");
  if (fp == NULL) {
    fprintf(stderr, "Can't open %s for writing: %s\n", filename,
            strerror(errno));
    exit(1);
  }

  fwrite(buf, 1, size, fp);
  fclose(fp);
}

int main(int argc, char **argv) {
  if (argc != 4) {
    fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n",
            argv[0]);
    exit(2);
  }

  int error;

  size_t input_size;
  char *input_bitmap_bytes = ReadFile(argv[1], &input_size);

  Image image;
  error = ParseBitmapImage(input_bitmap_bytes, input_size, &image);
  free(input_bitmap_bytes);
  if (error) {
    exit(1);
  }

  RotateBy(&image, atoi(argv[3]));

  char *output_bitmap_bytes;
  size_t size;
  error = WriteImageBitmap(&image, &output_bitmap_bytes, &size);
  FreeImageData(&image);
  if (error) {
    exit(1);
  }

  WriteFile(argv[2], output_bitmap_bytes, size);
  free(output_bitmap_bytes);
}
